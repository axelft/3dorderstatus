var mongoose = require('mongoose');

var orderSchema = new mongoose.Schema( {
    noComm: Number,
    nameFile: String,
    statusFile: String,
    progressFile: Number,
    eta: Date,
    addDate: Date
}),
    Order = mongoose.model('user', orderSchema);

module.exports = Order;