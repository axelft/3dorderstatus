var mongoose = require('mongoose');

var userSchema = new mongoose.Schema( {
    username: String,
    password: String
}),
    User = mongoose.model('user', userSchema);

module.exports = User;