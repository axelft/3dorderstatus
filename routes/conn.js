var express = require('express');
var router = express.Router();

var User = require('../models/User');

/*const crypto = require('crypto');
crypto.pbkdf2("J'h3rBAVV78", 'P456314654r654654z56e6854w6845z65n8645y', 100000, 512, 'sha512', (err, key) => {
    if (err) throw err;
    var addUser = new User({
        username: 'axel-ft',
        password: key.toString('hex')
    });

    addUser.save(function(err) {
        if (err) return console.error(err);
        console.log("Utilisateur enregistré");
    });
});*/

/* GET Connexion page. */
router.get('/', function(req, res, next) {
    res.render('conn', { title: 'Connexion' });
});

module.exports = router;
