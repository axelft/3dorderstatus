var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Suivi de commande 3D Hubs' });
});

module.exports = router;
